<?php


namespace DigitalBattlefield\Tests\TCAdmin2SDK;


use DigitalBattlefield\TCAdmin2SDK\Customer;
use DigitalBattlefield\TCAdmin2SDK\Service\GameSetings;
use DigitalBattlefield\TCAdmin2SDK\Service\Priority;
use DigitalBattlefield\TCAdmin2SDK\TCAdmin2;
use PHPUnit\Framework\TestCase;

class TCAdmin2Test extends TestCase
{
    public function testClientReturnsClient()
    {
        $client = new TCAdmin2($_ENV['tcadmin_host'], $_ENV['tcadmin_user'], $_ENV['tcadmin_pass']);
        $this->assertInstanceOf(TCAdmin2::class, $client);
    }

    public function testGetAvailableGames()
    {
        $client = new TCAdmin2($_ENV['tcadmin_host'], $_ENV['tcadmin_user'], $_ENV['tcadmin_pass']);

        $games = $client->getAvailableGames();

        $this->assertIsArray($games);
    }

    public function testGetAvailableVoice()
    {
        $client = new TCAdmin2($_ENV['tcadmin_host'], $_ENV['tcadmin_user'], $_ENV['tcadmin_pass']);

        $games = $client->getAvailableVoice();

        $this->assertIsArray($games);
    }
}