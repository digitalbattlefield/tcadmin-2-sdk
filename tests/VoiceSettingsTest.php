<?php


namespace DigitalBattlefield\Tests\TCAdmin2SDK;


use DigitalBattlefield\TCAdmin2SDK\Service\Startup;
use DigitalBattlefield\TCAdmin2SDK\Service\VoiceSettings;
use PHPUnit\Framework\TestCase;

class VoiceSettingsTest extends TestCase
{
    public function testVoiceSettings()
    {
        $voiceSettings = new VoiceSettings();
        $voiceSettings->setVoiceId(1);
        $voiceSettings->setBillingId('d3be8a64-0aa2-4f72-9371-881ae64f605c');
        $voiceSettings->setPublicSlots(8);
        $voiceSettings->setPrivateSlots(0);
        $voiceSettings->setDatacenter(1);
        $voiceSettings->setStartup(Startup::AUTOMATIC);

        $voiceSettings->setDownloadQuota(100);
        $voiceSettings->setUploadQuota(500);

        $vars = $voiceSettings->buildVars();

        $this->assertEquals($vars['voice_id'], 1);
        $this->assertEquals($vars['voice_package_id'], 'd3be8a64-0aa2-4f72-9371-881ae64f605c');
        $this->assertEquals($vars['voice_slots'], 8);
        $this->assertEquals($vars['voice_download_quota'], 100);
        $this->assertEquals($vars['voice_upload_quota'], 500);
        $this->assertEquals($vars['voice_startup'], "Automatic");
        $this->assertEquals($vars['voice_datacenter'], 1);

        $voiceSettings->setServer(1);

        $vars = $voiceSettings->buildVars();
        $this->assertTrue(!isset($vars['voice_datacenter']));
        $this->assertEquals($vars['voice_server'], 1);
    }
}