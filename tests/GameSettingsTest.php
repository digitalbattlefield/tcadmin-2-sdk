<?php


namespace DigitalBattlefield\Tests\TCAdmin2SDK;


use DigitalBattlefield\TCAdmin2SDK\Service\GameSetings;
use DigitalBattlefield\TCAdmin2SDK\Service\Priority;
use PHPUnit\Framework\TestCase;

class GameSettingsTest extends TestCase
{
    public function testGameSettings()
    {
        $serviceSettings = new GameSetings();

        $serviceSettings->setGameId(1);
        $serviceSettings->setBillingId('d3be8a64-0aa2-4f72-9371-881ae64f605c');
        $serviceSettings->setGamePublicSlots(8);
        $serviceSettings->setGamePrivateSlots(0);
        $serviceSettings->setGameIsBranded(false);
        $serviceSettings->setGameDatacenter(1);
        $serviceSettings->setStartAfterCreation(true);

        $serviceSettings->setHostName('Another TCAdmin Server');
        $serviceSettings->setPriority(Priority::ABOVE_NORMAL);
        $serviceSettings->setRconPassword('random');

        $gameVars = $serviceSettings->buildVars();

        $this->assertEquals($gameVars['game_id'], 1);
        $this->assertEquals($gameVars['game_package_id'], 'd3be8a64-0aa2-4f72-9371-881ae64f605c');
        $this->assertEquals($gameVars['game_slots'], 8);
        $this->assertEquals($gameVars['game_start'], 1);
        $this->assertEquals($gameVars['game_priority'], 'AboveNormal');
        $this->assertEquals($gameVars['game_datacenter'], 1);
        $this->assertEquals($gameVars['game_hostname'], 'Another TCAdmin Server');
        $this->assertEquals($gameVars['game_rcon_password'], 'random');

        $serviceSettings->setGameServer(1);

        $gameVars = $serviceSettings->buildVars();
        $this->assertTrue(!isset($gameVars['game_datacenter']));
        $this->assertEquals($gameVars['game_server'], 1);
    }

    public function testCustomVariables()
    {
        $serviceSettings = new GameSetings();

        $serviceSettings->setGameId(1);
        $serviceSettings->setBillingId('d3be8a64-0aa2-4f72-9371-881ae64f605c');
        $serviceSettings->setGamePublicSlots(8);
        $serviceSettings->setGamePrivateSlots(0);

        $serviceSettings->addCustomVariable('Xmx', 1024);
        $gameVars = $serviceSettings->buildVars();
        $this->assertEquals($gameVars['gamevar_Xmx'], 1024);

        $serviceSettings->removeCustomVariable('Xmx');
        $gameVars = $serviceSettings->buildVars();
        $this->assertTrue(!isset($gameVars['gamevar_Xmx']));
    }
}