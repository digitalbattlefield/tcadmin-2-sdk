<?php


namespace DigitalBattlefield\Tests\TCAdmin2SDK;


use DigitalBattlefield\TCAdmin2SDK\Customer;
use PHPUnit\Framework\TestCase;

class CustomerTest extends TestCase
{
    public function testSettingCustomerName()
    {
        $customer = new Customer();

        $customer->setCustomerName('Demo', 'User');

        $data = $customer->buildVars();

        $this->assertEquals($data['user_fname'], 'Demo');
        $this->assertEquals($data['user_lname'], 'User');
    }

    public function testSettingCustomerEmail()
    {
        $customer = new Customer();

        $customer->setEmail('foo@bar.com');

        $data = $customer->buildVars();

        $this->assertEquals($data['user_email'], 'foo@bar.com');
    }

    public function testSettingCustomerUsername()
    {
        $customer = new Customer();

        $customer->setUsername('demouser1');

        $data = $customer->buildVars();

        $this->assertEquals($data['user_name'], 'demouser1');
    }

    public function testSettingCustomerBillingID()
    {
        $customer = new Customer();

        $customer->setBillingId('user_1234');

        $data = $customer->buildVars();

        $this->assertEquals($data['client_id'], 'user_1234');
    }

    public function testSettingCustomerContactNumbers()
    {
        $customer = new Customer();

        $customer->setContactNumbers([
            'phone1' => 987654321,
            'phone2' => 123456789.
        ]);

        $data = $customer->buildVars();

        $this->assertEquals($data['user_phone1'], 987654321);
        $this->assertEquals($data['user_phone2'], 123456789);
    }

    public function testSettingCustomerAddress()
    {
        $customer = new Customer();

        $customer->setAddress([
            'line1' => 'Balance Servers, Inc',
            'line2' => '16 Smithson Drive',
            'city' => 'Beverly',
            'state' => 'Massachusetts',
            'country' => 'United States',
            'zip' => '01915',
        ]);

        $data = $customer->buildVars();

        $this->assertEquals($data['user_address1'], 'Balance Servers, Inc');
        $this->assertEquals($data['user_address2'], '16 Smithson Drive');
        $this->assertEquals($data['user_city'], 'Beverly');
        $this->assertEquals($data['user_state'], 'Massachusetts');
        $this->assertEquals($data['user_country'], 'United States');
        $this->assertEquals($data['user_zip'], '01915');
    }
}