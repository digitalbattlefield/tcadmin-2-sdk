<?php


namespace DigitalBattlefield\TCAdmin2SDK;


/**
 * Customer template for creating services;
 *
 * @package DigitalBattlefield\TCAdmin2SDK
 */
class Customer
{
    private $firstName;
    private $lastName;

    private $username;
    private $email;
    private $billingId;

    private $addressLine1;
    private $addressLine2;
    private $city;
    private $state;
    private $postcode;
    private $country;
    private $phone1;
    private $phone2;

    /**
     * Sets the customers name
     *
     * @param string $firstName
     * @param string $lastName
     * @return $this
     */
    public function setCustomerName($firstName, $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * Sets the customers billing panel id
     *
     * @param $billingId
     * @return $this;
     */
    public function setBillingId($billingId)
    {
        $this->billingId = $billingId;
        return $this;
    }

    /**
     * Sets the username the customer will sign into TCAdmin 2 with
     *
     * @param string $username
     * @return $this;
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Sets the customers TCAdmin 2 email address
     *
     * @param string $email
     * @return $this;
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Sets the customers TCAdmin 2 address
     * @param $address
     * @return $this
     */
    public function setAddress($address)
    {
        if (isset($address['line1'])) {
            $this->addressLine1 = $address['line1'];
        }

        if (isset($address['line2'])) {
            $this->addressLine2 = $address['line2'];
        }

        if (isset($address['city'])) {
            $this->city = $address['city'];
        }

        if (isset($address['state'])) {
            $this->state = $address['state'];
        }

        if (isset($address['country'])) {
            $this->country = $address['country'];
        }

        if (isset($address['zip'])) {
            $this->postcode = $address['zip'];
        }

        return $this;
    }

    /**
     * Sets the customers TCAdmin 2 phone numbers
     *
     * @param $numbers
     * @return $this
     */
    public function setContactNumbers($numbers)
    {
        if (isset($numbers['phone1'])) {
            $this->phone1 = $numbers['phone1'];
        }
        if (isset($numbers['phone2'])) {
            $this->phone2 = $numbers['phone2'];
        }

        return $this;
    }

    /**
     * Builds the array that will be used in requests
     *
     * @return array;
     */
    public function buildVars()
    {
        $data = [
            'user_name' => $this->username,
            'client_id' => $this->billingId,
            'user_email' => $this->email,
            'user_fname' => $this->firstName,
            'user_lname' => $this->lastName,
        ];

        if ($this->addressLine1) {
            $data['user_address1'] = $this->addressLine1;
        }

        if ($this->addressLine2) {
            $data['user_address2'] = $this->addressLine2;
        }

        if ($this->city) {
            $data['user_city'] = $this->city;
        }

        if ($this->state) {
            $data['user_state'] = $this->state;
        }

        if ($this->postcode) {
            $data['user_zip'] = $this->postcode;
        }

        if ($this->country) {
            $data['user_country'] = $this->country;
        }

        if ($this->phone1) {
            $data['user_phone1'] = $this->phone1;
        }

        if ($this->phone2) {
            $data['user_phone2'] = $this->phone2;
        }

        return $data;
    }
}