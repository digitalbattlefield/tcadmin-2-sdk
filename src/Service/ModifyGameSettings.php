<?php


namespace DigitalBattlefield\TCAdmin2SDK\Service;

class ModifyGameSettings extends ModifySettings
{
    /** @var int $gameSlotsPublic */
    private $gameSlotsPublic;
    /** @var int $gameSlotsPrivate */
    private $gameSlotsPrivate;
    /** @var bool $isBranded */
    private $isBranded;
    /** @var string $gameServerPriority */
    private $gameServerPriority;
    /** @var array $customVariables */
    private $customVariables = [];

    public function setGameSlotsPublic($slots)
    {
        $this->gameSlotsPublic = $slots;

        return $this;
    }

    public function getGameSlotsPublic()
    {
        return $this->gameSlotsPublic;
    }

    public function setGameSlotsPrivate($slots)
    {
        $this->gameSlotsPrivate = $slots;

        return $this;
    }

    public function getGameSlotsPrivate()
    {
        return $this->gameSlotsPrivate;
    }

    public function setIsBranded($isBranded)
    {
        $this->isBranded = $isBranded;

        return $this;
    }

    public function getIsBranded()
    {
        return $this->isBranded;
    }

    public function setGameServerPriority($priority)
    {
        $this->gameServerPriority = $priority;

        return $this;
    }

    public function getGameServerPriority()
    {
        return $this->gameServerPriority;
    }

    /**
     * Adds a custom variable to the service
     *
     * Add a custom variable to the service, for example Minecraft needs Xmx for max ram,
     * this function automatically adds gamevar_ to the start
     *
     * @param string $key The custom variable after the gamevar_
     * @param {*} $value The value of the custom variable
     * @return $this
     */
    public function addCustomVariable($key, $value)
    {
        $this->customVariables['gamevar_' . $key] = $value;
        return $this;
    }

    /**
     * Removes a custom variable from the service by key
     *
     * @param string $key
     * @return bool
     */
    public function removeCustomVariable($key)
    {
        if (isset($this->customVariables['gamevar_' . $key])) {
            unset($this->customVariables['gamevar_' . $key]);
            return true;
        }

        return false;
    }

    public function buildVars()
    {
        $vars = [];

        if ($this->gameSlotsPublic) {
            $vars['game_slots'] = $this->gameSlotsPublic;
        }

        if ($this->gameSlotsPrivate) {
            $vars['game_private'] = $this->gameSlotsPrivate;
        }

        if ($this->isBranded !== null) {
            $vars['game_branded'] = $this->isBranded;
        }

        if ($this->gameServerPriority) {
            $vars['game_priority'] = $this->gameServerPriority;
        }
        if (count($this->customVariables)) {
            $vars = array_merge($vars, $this->customVariables);
        }

        return $vars;
    }
}
