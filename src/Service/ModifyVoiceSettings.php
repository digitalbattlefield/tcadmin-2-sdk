<?php


namespace DigitalBattlefield\TCAdmin2SDK\Service;

class ModifyVoiceSettings extends ModifySettings
{
    /** @var int $voiceSlotsPublic */
    private $voiceSlotsPublic;
    /** @var int $voiceSlotsPrivate */
    private $voiceSlotsPrivate;
    /** @var bool $isBranded */
    private $isBranded;
    /** @var int $voiceUploadQuote */
    private $voiceUploadQuote;
    /** @var int $voiceDownloadQuote */
    private $voiceDownloadQuote;
    /** @var string $voiceServerPriority */
    private $voiceServerPriority;

    public function setVoiceSlotsPublic($slots)
    {
        $this->voiceSlotsPublic = $slots;

        return $this;
    }

    public function getVoiceSlotsPublic()
    {
        return $this->voiceSlotsPublic;
    }

    public function setVoiceSlotsPrivate($slots)
    {
        $this->voiceSlotsPrivate = $slots;

        return $this;
    }

    public function getVoiceSlotsPrivate()
    {
        return $this->voiceSlotsPrivate;
    }

    public function setVoiceUploadQuote($quota)
    {
        $this->voiceUploadQuote = $quota;

        return $this;
    }

    public function getVoiceDownloadQuote()
    {
        return $this->voiceUploadQuote;
    }

    public function setVoiceDownloadQuote($quota)
    {
        $this->voiceDownloadQuote = $quota;

        return $this;
    }

    public function getVoiceUploadQuote()
    {
        return $this->voiceDownloadQuote;
    }

    public function setIsBranded($isBranded)
    {
        $this->isBranded = $isBranded;

        return $this;
    }

    public function getIsBranded()
    {
        return $this->isBranded;
    }

    public function setVoiceServerPriority($priority)
    {
        $this->voiceServerPriority = $priority;

        return $this;
    }

    public function getVoiceServerPriority()
    {
        return $this->voiceServerPriority;
    }

    public function buildVars()
    {
        $vars = [];

        if ($this->voiceSlotsPublic) {
            $vars['voice_slots'] = $this->voiceSlotsPublic;
        }

        if ($this->voiceSlotsPrivate) {
            $vars['voice_private'] = $this->voiceSlotsPrivate;
        }

        if ($this->isBranded !== null) {
            $vars['voice_branded'] = $this->isBranded;
        }

        if ($this->voiceServerPriority) {
            $vars['voice_priority'] = $this->voiceServerPriority;
        }

        if ($this->voiceUploadQuote) {
            $vars['voice_upload_quota'] = $this->voiceUploadQuote;
        }

        if ($this->voiceDownloadQuote) {
            $vars['voice_download_quota'] = $this->voiceDownloadQuote;
        }

        return $vars;
    }
}
