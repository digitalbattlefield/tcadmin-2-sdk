<?php


namespace DigitalBattlefield\TCAdmin2SDK\Service;


abstract class Settings
{
    public $serviceType;

    /**
     * Builds the array that will be used in requests
     *
     * @return array;
     */
    abstract public function buildVars();
}