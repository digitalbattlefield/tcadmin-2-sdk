<?php


namespace DigitalBattlefield\TCAdmin2SDK\Service;

/**
 * Startup variables for creating services
 *
 * @package DigitalBattlefield\TCAdmin2SDK\Service
 */
class Startup
{
    public const AUTOMATIC = 'Automatic';
    public const MANUAL = 'Manual';
    public const DISABLED = 'Disabled';
}