<?php


namespace DigitalBattlefield\TCAdmin2SDK\Service;


abstract class ModifySettings
{
    /**
     * Builds the array that will be used in requests
     *
     * @return array;
     */
    abstract public function buildVars();
}