<?php


namespace DigitalBattlefield\TCAdmin2SDK\Service;

/**
 * Game server settings class
 * 
 * @TODO: Doc type all of the private variables
 */
class GameSetings extends Settings
{
    private $gameId;
    private $gamePublicSlots;
    private $gamePrivateSlots;
    private $gameIsBranded;
    private $startAfterCreation;
    private $priority;
    private $startUp;
    private $gameDatacenter;
    private $gameServer;
    private $hostName;
    private $rconPassword;
    private $gamePassword;
    private $customVariables = [];
    private $billingId;

    /**
     * Game server settings
     * 
     * @TODO: Allow users to pass an array of variables to the constructor
     */
    public function __construct()
    {
        $this->serviceType = 'game';
    }

    /**
     * Sets the ID of the game in TCAdmin 2
     * @param mixed $gameId
     * @return $this
     */
    public function setGameId($gameId)
    {
        $this->gameId = $gameId;
        return $this;
    }

    /**
     * Sets the game servers billing panel id
     *
     * @param mixed $billingId
     * @return $this
     */
    public function setBillingId($billingId)
    {
        $this->billingId = $billingId;
        return $this;
    }

    /**
     * Sets the number of public slots
     *
     * @param mixed $gamePublicSlots
     * @return $this
     */
    public function setGamePublicSlots($gamePublicSlots)
    {
        $this->gamePublicSlots = $gamePublicSlots;
        return $this;
    }

    /**
     * Sets the number of private slots
     *
     * @param mixed $gamePrivateSlots
     * @return $this
     */
    public function setGamePrivateSlots($gamePrivateSlots)
    {
        $this->gamePrivateSlots = $gamePrivateSlots;
        return $this;
    }

    /**
     * Sets whether the server requires the company name to be displayed
     *
     * @param mixed $gameIsBranded
     * @return $this
     */
    public function setGameIsBranded($gameIsBranded)
    {
        $this->gameIsBranded = (int)$gameIsBranded;
        return $this;
    }

    /**
     * Sets the datacenter that the game will be provisioned in
     *
     * Setting this resets the game server host id to null
     *
     * @param mixed $gameDatacenter
     * @return $this
     */
    public function setGameDatacenter($gameDatacenter)
    {
        if ($this->gameServer !== null) {
            $this->gameServer = null;
        }

        $this->gameDatacenter = $gameDatacenter;
        return $this;
    }

    /**
     * Sets the ID of the host server that the game will be provisioned on
     *
     * Setting this resets the data center id to null
     *
     * @param mixed $gameServer
     * @return $this
     */
    public function setGameServer($gameServer)
    {
        if ($this->gameDatacenter !== null) {
            $this->gameDatacenter = null;
        }

        $this->gameServer = $gameServer;
        return $this;
    }

    /**
     * Sets whether the game server will start after creation
     *
     * @param int|bool $startAfterCreation
     * @return $this
     */
    public function setStartAfterCreation($startAfterCreation)
    {
        $this->startAfterCreation = (int)$startAfterCreation;
        return $this;
    }

    /**
     * Sets sets the password of the game server if available
     *
     * @param string $gamePassword
     * @return $this
     */
    public function setGamePassword($gamePassword)
    {
        $this->gamePassword = $gamePassword;
        return $this;
    }

    /**
     * Sets the hostname the server will display if available
     *
     * @param string $hostName
     * @return $this
     */
    public function setHostName($hostName)
    {
        $this->hostName = $hostName;
        return $this;
    }

    /**
     * Sets the CPU priority of the game server
     *
     * @param string $priority
     * @return $this
     * @see Priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * Sets the RCON password of the game server if available
     *
     * @param mixed $rconPassword
     * @return $this
     */
    public function setRconPassword($rconPassword)
    {
        $this->rconPassword = $rconPassword;
        return $this;
    }

    /**
     * Adds a custom variable to the service
     *
     * Add a custom variable to the service, for example Minecraft needs Xmx for max ram,
     * this function automatically adds gamevar_ to the start
     *
     * @param string $key The custom variable after the gamevar_
     * @param {*} $value The value of the custom variable
     * @return $this
     */
    public function addCustomVariable($key, $value)
    {
        $this->customVariables['gamevar_' . $key] = $value;
        return $this;
    }

    /**
     * Removes a custom variable from the service by key
     *
     * @param string $key
     * @return bool
     */
    public function removeCustomVariable($key)
    {
        if (isset($this->customVariables['gamevar_' . $key])) {
            unset($this->customVariables['gamevar_' . $key]);
            return true;
        }

        return false;
    }

    /**
     * Sets whether the game server will automatically restart after a host reboot
     *
     * @param mixed $startUp
     * @return $this
     * @see Startup
     */
    public function setStartUp($startUp)
    {
        $this->startUp = $startUp;
        return $this;
    }

    /**
     * Builds the array that will be used in requests
     *
     * @return array;
     */
    public function buildVars()
    {
        $data = [
            'game_id' => $this->gameId,
            'game_package_id' => $this->billingId,
            'game_slots' => $this->gamePublicSlots !== null ? $this->gamePublicSlots : 0,
        ];

        if ($this->gamePrivateSlots) {
            $data['game_private'] = $this->gamePrivateSlots;
        }

        if ($this->gameIsBranded) {
            $data['game_branded'] = $this->gameIsBranded;
        }

        if ($this->startAfterCreation) {
            $data['game_start'] = $this->startAfterCreation;
        }

        if ($this->priority) {
            $data['game_priority'] = $this->priority;
        }

        if ($this->startUp) {
            $data['game_startup'] = $this->startUp;
        }

        if ($this->gameDatacenter) {
            $data['game_datacenter'] = $this->gameDatacenter;
        }

        if ($this->gameServer) {
            $data['game_server'] = $this->gameServer;
        }

        if ($this->hostName) {
            $data['game_hostname'] = $this->hostName;
        }

        if ($this->rconPassword) {
            $data['game_rcon_password'] = $this->rconPassword;
        }

        if ($this->gamePassword) {
            $data['game_private_password'] = $this->gamePassword;
        }

        if (count($this->customVariables)) {
            $data = array_merge($data, $this->customVariables);
        }

        return $data;
    }
}