<?php


namespace DigitalBattlefield\TCAdmin2SDK\Service;

/**
 * Priority variables for creating services
 *
 * @package DigitalBattlefield\TCAdmin2SDK\Service
 */
class  Priority
{
    public const IDLE = 'Idle';
    public const BELOW_NORMAL = 'BelowNormal';
    public const ABOVE_NORMAL = 'AboveNormal';
    public const NORMAL = 'Normal';
    public const HIGH = 'High';
    public const REAL_TIME = 'RealTime';
}