<?php


namespace DigitalBattlefield\TCAdmin2SDK\Service;


class VoiceSettings extends Settings
{
    private $voiceId;
    private $billingId;
    private $publicSlots;
    private $privateSlots;
    private $uploadQuota;
    private $downloadQuota;
    private $priority;
    private $startup;
    private $datacenter;
    private $server;
    private $hostname;
    private $rconPassword;
    private $privatePassword;
    private $customVariables = [];

    public function __construct()
    {
        $this->serviceType = 'voice';
    }

    /**
     * @param mixed $voiceId
     * @return $this;
     */
    public function setVoiceId($voiceId)
    {
        $this->voiceId = $voiceId;
        return $this;
    }

    /**
     * @param mixed $billingId
     * @return $this;
     */
    public function setBillingId($billingId)
    {
        $this->billingId = $billingId;
        return $this;
    }

    /**
     * @param mixed $publicSlots
     * @return $this;
     */
    public function setPublicSlots($publicSlots)
    {
        $this->publicSlots = $publicSlots;
        return $this;
    }

    /**
     * @param mixed $privateSlots
     * @return $this;
     */
    public function setPrivateSlots($privateSlots)
    {
        $this->privateSlots = $privateSlots;
        return $this;
    }

    /**
     * @param mixed $uploadQuota
     * @return $this;
     */
    public function setUploadQuota($uploadQuota)
    {
        $this->uploadQuota = $uploadQuota;
        return $this;
    }

    /**
     * @param mixed $downloadQuota
     * @return $this;
     */
    public function setDownloadQuota($downloadQuota)
    {
        $this->downloadQuota = $downloadQuota;
        return $this;
    }

    /**
     * Sets the CPU priority of the game server
     *
     * @param mixed $priority
     * @return $this
     * @see Priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * Sets whether the game server will automatically restart after a host reboot
     *
     * @param mixed $startUp
     * @return $this
     * @see Startup
     */
    public function setStartup($startup)
    {
        $this->startup = $startup;
        return $this;
    }

    /**
     * Sets the data center that the voice server will be provisioned in
     *
     * Setting this resets the voice server host id to null
     *
     * @param mixed $gameDatacenter
     * @return $this
     */
    public function setDatacenter($datacenter)
    {
        if ($this->server) {
            $this->server = null;
        }

        $this->datacenter = $datacenter;
        return $this;
    }

    /**
     * Sets the ID of the host server that the voice server will be provisioned on
     *
     * Setting this resets the data center id to null
     *
     * @param mixed $gameServer
     * @return $this
     */
    public function setServer($server)
    {
        if ($this->datacenter) {
            $this->datacenter = null;
        }

        $this->server = $server;
        return $this;
    }

    /**
     * @param mixed $hostname
     * @return $this;
     */
    public function setHostname($hostname)
    {
        $this->hostname = $hostname;
        return $this;
    }

    /**
     * @param mixed $rconPassword
     * @return $this;
     */
    public function setRconPassword($rconPassword)
    {
        $this->rconPassword = $rconPassword;
        return $this;
    }

    /**
     * @param mixed $privatePassword
     * @return $this;
     */
    public function setPrivatePassword($privatePassword)
    {
        $this->privatePassword = $privatePassword;
        return $this;
    }

    /**
     * Adds a custom variable to the service
     *
     * Add a custom variable to the service, for example Minecraft needs Xmx for max ram,
     * this function automatically adds voicevar_ to the start
     *
     * @param string $key The custom variable after the voicevar_
     * @param {*} $value The value of the custom variable
     * @return $this
     */
    public function addCustomVariable($key, $value)
    {
        $this->customVariables['voicevar_' . $key] = $value;
        return $this;
    }

    /**
     * Removes a custom variable from the service by key
     *
     * @param string $key
     * @return bool
     */
    public function removeCustomVariable($key)
    {
        if (isset($this->customVariables['voicevar_' . $key])) {
            unset($this->customVariables['voicevar_' . $key]);
            return true;
        }

        return false;
    }

    /**
     * Builds the array that will be used in requests
     *
     * @return array;
     */
    public function buildVars()
    {
        $data = [
            'voice_id' => $this->voiceId,
            'voice_package_id' => $this->billingId,
            'voice_slots' => $this->publicSlots !== null ? $this->publicSlots : 0
        ];

        if ($this->privateSlots) {
            $data['voice_private'] = $this->privateSlots;
        }

        if ($this->uploadQuota) {
            $data['voice_upload_quota'] = $this->uploadQuota;
        }

        if ($this->downloadQuota) {
            $data['voice_download_quota'] = $this->downloadQuota;
        }

        if ($this->priority) {
            $data['voice_priority'] = $this->priority;
        }

        if ($this->startup) {
            $data['voice_startup'] = $this->startup;
        }

        if ($this->datacenter) {
            $data['voice_datacenter'] = $this->datacenter;
        }

        if ($this->server) {
            $data['voice_server'] = $this->server;
        }

        if ($this->hostname) {
            $data['voice_hostname'] = $this->hostname;
        }

        if ($this->rconPassword) {
            $data['voice_rcon_password'] = $this->rconPassword;
        }

        if ($this->privatePassword) {
            $data['voice_private_password'] = $this->privatePassword;
        }

        if (count($this->customVariables)) {
            $data = array_merge($data, $this->customVariables);
        }

        return $data;
    }
}