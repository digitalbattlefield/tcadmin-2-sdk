<?php

namespace DigitalBattlefield\TCAdmin2SDK;

use DigitalBattlefield\TCAdmin2SDK\Service\Settings;
use DigitalBattlefield\TCAdmin2SDK\Service\ModifySettings;

class TCAdmin2
{

    /*
     * Error Constants
     */
    const ERROR_NONE = 0;
    const ERROR_MISSINGCURL = 1;
    const ERROR_CONNECTSTRING = 2;
    const ERROR_CURLCMDFAILED = 3;
    const ERROR_CURLRESPONSEINVALID = 4;

    /**
     * @var string
     */
    private $panelUrl;
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $password;

    /**
     * @var int
     */
    private $timeout;

    const RESPONSE_TYPE_XML = 'xml';
    const RESPONSE_TYPE_TEXT = 'text';

    const FIELD_USERNAME = 'tcadmin_username';
    const FIELD_PASSWORD = 'tcadmin_password';
    const FIELD_FUNCTION = 'function';
    const FIELD_RETURN_TYPE = 'response_type';

    const CMD_GET_GAMESERVERS = 'GetSupportedGames';
    const CMD_GET_VOICESERVERS = 'GetSupportedVoiceServers';
    const CMD_ADD_SETUP = 'AddPendingSetup';
    const CMD_SUSPEND_SERVICES = 'SuspendGameAndVoiceByBillingID';
    const CMD_UNSUSPEND_SERVICES = 'UnSuspendGameAndVoiceByBillingID';
    const CMD_DELETE_SERVICES = 'DeleteGameAndVoiceByBillingID';
    const CMD_UPDATE_SETTINGS = 'UpdateSettings';
    const CMD_UPDATE_PASSWORD = 'ChangePassword';

    /*
     * Status settings
     */
    const SETUP_PENDING = 1;
    const SETUP_COMPLETE = 2;
    const SETUP_ERRORED = 3;

    /*
     * Billing status settings
     */
    const BILLING_STATUS_ACTIVE = 1;
    const BILLING_STATUS_SUSPENDED = 2;

    /**
     * TCAdmin 2 Client constructor
     *
     * @param string $panelUrl The URL for your TCAdmin 2 installation
     * @param string $username The TCAdmin username to use for API requests
     * @param string $password The password for your TCAdmin user
     * @param int $timeout TCAdmin panel connection timeout
     */
    public function __construct($panelUrl, $username, $password, $timeout = 2)
    {
        $this->panelUrl = $panelUrl;
        $this->username = $username;
        $this->password = $password;
        $this->timeout = $timeout;
    }

    /**
     * Get all of the games supported by the TCAdmin 2 installation
     *
     * @return array
     * @throws TCAdmin2Exception
     */
    public function getAvailableGames()
    {
        $gamesXml = $this->_remoteCall(self::CMD_GET_GAMESERVERS);

        $games = [];

        foreach ($gamesXml->game as $game) {
            $games[] = [
                'id' => $game->gameid,
                'name' => $game->name
            ];
        }

        return $games;
    }

    /**
     * Get all of the games supported by the TCAdmin 2 installation
     *
     * @return array
     * @throws TCAdmin2Exception
     */
    public function getAvailableVoice()
    {
        $gamesXml = $this->_remoteCall(self::CMD_GET_VOICESERVERS);

        $games = [];

        foreach ($gamesXml->game as $game) {
            $games[] = [
                'id' => $game->gameid,
                'name' => $game->name
            ];
        }

        return $games;
    }

    /**
     * Creates a pending setup with the information provided from the
     * customer and the service settings parameters
     *
     * @param Customer $customer
     * @param Settings $settings
     * @return \SimpleXMLElement
     * @throws TCAdmin2Exception
     */
    public function createService(Customer $customer, Settings $settings)
    {
        $data = array_merge($customer->buildVars(), $settings->buildVars());

        return $this->_remoteCall(self::CMD_ADD_SETUP, $data);
    }

    /**
     * Modifies an existing service with the provided settings
     * 
     * @param mixed $billingId
     * @param ModifySettings $settings
     * @return \SimpleXMLElement
     * @throws TCAdmin2Exception
     */
    public function modifyService($billingId, ModifySettings $settings)
    {
        $data = array_merge(['client_package_id' => $billingId], $settings->buildVars());

        return $this->_remoteCall(self::CMD_UPDATE_SETTINGS, $data);
    }

    /**
     * Destroy's the server with the billing id
     *
     * @param $billingId
     * @return \SimpleXMLElement
     * @throws TCAdmin2Exception
     */
    public function destroyService($billingId)
    {
        return $this->_remoteCall(self::CMD_DELETE_SERVICES, ['client_package_id' => $billingId]);
    }

    /**
     * Suspend the server with the billing id
     *
     * @param $billingId
     * @return \SimpleXMLElement
     * @throws TCAdmin2Exception
     */
    public function suspendService($billingId)
    {
        return $this->_remoteCall(self::CMD_SUSPEND_SERVICES, ['client_package_id' => $billingId]);
    }

    /**
     * Unsuspend the server with the billing id
     *
     * @param $billingId
     * @return \SimpleXMLElement
     * @throws TCAdmin2Exception
     */
    public function resumeService($billingId)
    {
        return $this->_remoteCall(self::CMD_UNSUSPEND_SERVICES, ['client_package_id' => $billingId]);
    }

    /**
     * @param array $data
     * @return array
     */
    protected function _getBody($data = [])
    {
        $body = [
            self::FIELD_USERNAME => $this->username,
            self::FIELD_PASSWORD => $this->password,
            self::FIELD_RETURN_TYPE => self::RESPONSE_TYPE_XML
        ];

        return array_merge($data, $body);
    }

    /**
     * @param string $function
     * @param array $data
     * @return \SimpleXMLElement
     * @throws TCAdmin2Exception
     */
    protected function _remoteCall($function, $data = [])
    {
        // Init cURL
        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => $this->panelUrl,
            CURLOPT_CONNECTTIMEOUT => $this->timeout,
            CURLOPT_TIMEOUT => $this->timeout,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_MAXREDIRS => 4,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array_merge($this->_getBody($data), [self::FIELD_FUNCTION => $function]),
        ));

        if (!$result = curl_exec($ch)) {
            throw new TCAdmin2Exception(curl_error($ch), self::ERROR_CURLCMDFAILED);
        }

        // Close cURL connection
        curl_close($ch);

        if (($xml = simplexml_load_string($result)) === false) {
            throw new TCAdmin2Exception('Unable to parse return as XML.', self::ERROR_CURLRESPONSEINVALID);
        }

        return $xml;
    }
}
